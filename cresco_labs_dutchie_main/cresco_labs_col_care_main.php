<?php
ini_set('memory_limit', '128M');

class cresco_labs_col_care_main extends Vtx_Service_Plugin
{
    public $colHeaders;
    private $usage;
    private $count;

    public function __construct()
    {
        parent::__construct();

        $this->colHeaders = [
            "Dispensary Name",
            "Location Of Dispensary",
            "Category",
            "Brand Name",
            "Product Title",
            "THC",
            "CBD",
            "Description",
            "Rating",
            "Number of reviews",
            "Weight",
            "Price",
            "Price Includes Tax (Y/N)",
            "Medical or Recreational/Adult-use Pricing",
            "URL",
            "Usage (Medical or Recreational)",
            "Strain",
            "Discount Percent",
        ];

    }

    public function newSession()
    {
        echo ("==> New Session <== \n");
        $this->resetCurl(true);
        // $this->enableHola('US');

        $this->useProxy = true;

        $this->setHeaders('Connection', 'keep-alive');
        $this->setHeaders('content-type', 'application/x-www-form-urlencoded');
        $this->setHeaders('Accept', '*/*');
        $this->setHeaders('Origin', 'https://www.iheartjane.com');
        $this->setHeaders('Sec-Fetch-Site', 'cross-site');
        $this->setHeaders('Sec-Fetch-Mode', 'cors');
        $this->setHeaders('Sec-Fetch-Dest', 'empty');

        $this->setHeaders('Accept-Language', 'en-US,en;q=0.9,fr;q=0.8,ne;q=0.7,pt;q=0.6');
        $this->setHeaders('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');

        //$this->setCurlOption(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        //$this->setCurlOption(CURLOPT_CONNECTTIMEOUT, 180);
        //$this->setCurlOption(CURLOPT_TIMEOUT, 180);
    }

    public function main($params)
    {

    }

    public function run($stores, $name, $tax)
    {
        $url = 'https://vfm4x0n23a-3.algolianet.com/1/indexes/*/queries?x-algolia-agent=Algolia%20for%20JavaScript%20(4.2.0)%3B%20Browser%20(lite)%3B%20JS%20Helper%20(3.1.1)%3B%20react%20(16.13.1)%3B%20react-instantsearch%20(6.4.0)&x-algolia-api-key=b499e29eb7542dc373ec0254e007205d&x-algolia-application-id=VFM4X0N23A';

        $prods = [
            'Flower' => 'flower',
            'Vape' => 'vape',
            'Extract' => 'extract',
            'Tincture' => 'tincture',
            'Topical' => 'topical',
            'Captules/Edible' => 'edible',
            'Gear' => 'gear',
        ];

        foreach ($stores as $loco => $storeID) {

            $this->storeID = $storeID[0];
            if ($storeID[0] == 1346) {
                $this->usage = 'Recreational';
            } else {
                $this->usage = 'Medical';
            }
            $this->url = $storeID[1];
            foreach ($prods as $cat => $catVal) {
                $this->newSession();
                $this->switchProxy();

                $postParams = '{"requests":[{"indexName":"menu-products-production","params":"highlightPreTag=%3Cais-highlight-0000000000%3E&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&page=0&hitsPerPage=100&filters=root_types%3A%22' . $catVal . '%22%20AND%20store_id%20%3D%20' . $storeID[0] . '%20AND%20kind%3A%22' . $catVal . '%22&facets=%5B%5D&tagFilters="}]}';

                $res = $this->requestPost($url, $postParams)->getBody();
                $res = $this->jsonDecode($res);

                $result = $res['results'][0]['hits'];
                echo ("\t" . count($result) . " -- $cat Found, location:$loco \n\n");

                foreach ($result as $i => $item) {
                    echo ("Processing Item:" . ($i + 1) . " out of " . count($result) . " -- $loco \n");
                    $this->processDetails($item, $loco, $cat, $name, $tax);
                }
            }
        }

    }

    private function processDetails($product, $location, $cat, $name, $tax)
    {

        $arr = $this->dataSet->getEmptyRow();

        $arr['Dispensary Name'] = $name;

        $arr['Location Of Dispensary'] = trim(reset(explode('-', $location)));

        $arr['Category'] = $cat;

        $arr['Brand Name'] = $product['brand'];

        $arr['Product Title'] = $product['name'];

        $arr['Strain'] = $product['category'];

        $arr["THC"] = $product['percent_thc'] ? $product['percent_thc'] . '%' : "";

        $arr['CBD'] = $product['percent_cbd'] ? $product['percent_cbd'] . '%' : "";

        $desc = $product['brand_subtype'];

        if (!trim($desc)) {
            $desc = $product['description'];
        }

        if (Vtx_Text_Helper::allInOneLine($desc) == 'No description available. If you have any info on this strain, drop us some knowledge at strains@iheartjane.com') {
            $desc = '';
        }

        $arr["Rating"] = $product['aggregate_rating'];
        $arr["Number of reviews"] = $product['review_count'];

        $arr['Description'] = $desc;

        $arr['URL'] = $this->url;

        $arr['Image URL'] = reset($product['product_photos'][0]['urls']);

        if (!trim($arr['Image URL'])) {
            $arr['Image URL'] = $product['image_urls'][0];
        }

        $arr['Price Includes Tax (Y/N)'] = $tax;
        $arr['Usage (Medical or Recreational)'] = $this->usage;

        $weight = $product['available_weights'];
        if (count($weight)) {
            foreach ($weight as $wt) {
                $arr['Weight'] = $wt;
                $price = explode(' ', $wt);
                $price = implode('_', $price);
                $discountedPrice = (float) $product['discounted_price_' . $price];
                if ($discountedPrice > 0) {
                    $totalPrice = (float) $product['price_' . $price];
                    $disPer = ($totalPrice - $discountedPrice) / $totalPrice;
                    $disPer = $disPer * 100;
                    $arr['Discount Percent'] = $disPer . "%";
                    $arr['Price'] = '$' . $discountedPrice;

                } else {
                    $arr['Price'] = '$' . $product['price_' . $price];
                }

                $this->dataSet->addRow($arr);

            }
        } else {
            $discountedPrice = (float) $product['discounted_price_each'];
            if ($discountedPrice > 0) {
                $totalPrice = (float) $product['price_each'];
                $disPer = ($totalPrice - $discountedPrice) / $totalPrice;
                $disPer = $disPer * 100;
                $arr['Discount Percent'] = $disPer . "%";
                $arr['Price'] = '$' . $discountedPrice;
            } else {
                $arr['Price'] = '$' . $product['price_each'];
            }
            $this->dataSet->addRow($arr);
        }
    }
}
