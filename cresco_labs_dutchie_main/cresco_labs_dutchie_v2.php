<?php
//ini_set('memory_limit', '256M');

class cresco_labs_dutchie_v2 extends Vtx_Service_Plugin {
	public $colHeaders;

	public function __construct() {
		parent::__construct();

		$this->colHeaders = [
			"Dispensary Name",
			"Location Of Dispensary",
			"Category",
			"Brand Name",
			"Product Title",
			"THC",
			"CBD",
			"Description",
			"Rating",
			"Number of reviews",
			"Weight",
			"Price",
			"Price Includes Tax (Y/N)",
			"Medical or Recreational/Adult-use Pricing",
			"URL",
			"Usage (Medical or Recreational)",
			"Strain",
			"Discount Percent",
			"Image URL",
		];

		//$this->cache = new Vtx_Service_Data_Cache(__CLASS__);
	}

	private function newSession() {
		$this->resetCurl(true);

		$this->useProxy = true;
		// $this->setHeaders('Accept-Encoding', 'gzip, deflate, br');
		$this->setHeaders('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
		$this->setHeaders('Accept-Language', 'en-US,en;q=0.5');
		$this->setHeaders('Connection', 'keep-alive');
		$this->setHeaders('Upgrade-Insecure-Requests', '1');
		$this->setHeaders('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');

		$this->setCurlOption(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		$this->setCurlOption(CURLOPT_CONNECTTIMEOUT, 250);
		$this->setCurlOption(CURLOPT_TIMEOUT, 250);
	}

	public function main($params) {

	}

	public function run($apiLink, $detailsAPI, $slug, $name, $location, $type = '') {
		$this->newSession();
		$this->switchProxy();
		$try = 1;
		do {
			echo ("Processing GraphQL API: $try -- $name, $location \n");
			$res = $this->requestGet($apiLink)->getBody();
			print_r($res);
			die();
			$res = $this->jsonDecode($res);
			$products = $res['data']['filteredProducts']['products'];
			if ($products) {
				break;
			}
			$try++;
			$this->newSession();
			$this->switchProxy();
		} while ($try <= 5);

		foreach ($products as $pi => $product) {

			echo ("Processing Product: " . ($pi + 1) . " out of " . count($products) . "\n");

			$arr = $this->dataSet->getEmptyRow();

			$arr['Dispensary Name'] = $name;

			$arr['Location Of Dispensary'] = trim(reset(explode('-', $location)));

			$arr['Category'] = $product['type'];

			$arr['Brand Name'] = $product['brandName'];

			$arr['Product Title'] = $product['Name'];

			$arr['Rating'] = "";

			$arr['Number of reviews'] = "";

			$detailUrl = sprintf($detailsAPI, $product['_id']);
			$arr['Description'] = $this->processDescription($detailUrl);

			$arr['Image URL'] = $product['Image'];

			$thcUnit = $product['THCContent']['unit'];
			if ($thcUnit == 'PERCENTAGE') {
				$thcUnit = '%';
			} else if ($thcUnit == 'MILLIGRAMS') {
				$thcUnit = 'mg';
			}

			$thc = trim($product['THCContent']['value'] . ' ' . $thcUnit);
			if ($thc == '%' || $thc == 'mg') {
				$arr['THC'] = '';
			} else {
				$arr['THC'] = $thc;
			}

			$cbdUnit = $product['CBDContent']['unit'];
			if ($cbdUnit == 'PERCENTAGE') {
				$cbdUnit = '%';
			} else if ($cbdUnit == 'MILLIGRAMS') {
				$cbdUnit = 'mg';
			}

			$cbd = trim($product['CBDContent']['value'] . ' ' . $cbdUnit);
			if ($cbd == '%' || $cbd == 'mg') {
				$arr['CBD'] = '';
			} else {
				$arr['CBD'] = $cbd;
			}

			if ($type != '') {
				$arr['Usage (Medical or Recreational)'] = $type;
			} else {
				$arr['Usage (Medical or Recreational)'] = $product['medicalOnly'] ? "Medical" : "Recreational";
			}

			$strain = $product['strainType'];
			$arr['Strain'] = ($strain == 'N/A') ? '' : $strain;

			$arr['URL'] = "https://dutchie.com/embedded-menu/$slug/menu/$product[cName]";

			if ($product['specialData']['percentDiscount']) {
				$arr['Discount Percent'] = $product['specialData']['discount'] . "%";
			}
			foreach ($product['Options'] as $oi => $opt) {
				$wt = $product['Options'][$oi];

				if (preg_match('/([0-9]|\.)/', substr(trim($wt), 0, 1))) {
					$arr['Weight'] = $wt;
				}

				if ($product['recSpecialPrices'][$oi]) {
					$arr['Price'] = '$' . $product['recSpecialPrices'][$oi];
					$arr['Medical or Recreational/Adult-use Pricing'] = '$' . $product['recSpecialPrices'][$oi];
				} else {
					$arr['Price'] = '$' . $product['Prices'][$oi];
					$arr['Medical or Recreational/Adult-use Pricing'] = '$' . $product['recPrices'][$oi];
				}

				if ($arr['Price']) {
					print_r($arr);
					$this->dataSet->addRow($arr);
				}
			}
		}
	}

	private function processDescription($url) {
		$try = 1;
		do {
			echo ("Retrying for description: $try \n");
			$res = $this->requestGet($url)->getBody();
			$res = $this->jsonDecode($res);
			$description = strip_tags($res['data']['filteredProducts']['products'][0]['Description']);
			if (trim($description)) {
				break;
			}
			$try++;
			$this->newSession();
			$this->switchProxy();

		} while ($try <= 5);

		return $description;

	}
}