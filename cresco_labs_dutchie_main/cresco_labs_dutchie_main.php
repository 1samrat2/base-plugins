<?php
//ini_set('memory_limit', '256M');

class cresco_labs_dutchie_main extends Vtx_Service_Plugin
{
    public $colHeaders;

    public function __construct()
    {
        parent::__construct();

        $this->colHeaders = [
            "Dispensary Name",
            "Location Of Dispensary",
            "Category",
            "Brand Name",
            "Product Title",
            "THC",
            "CBD",
            "Description",
            "Rating",
            "Number of reviews",
            "Weight",
            "Price",
            "Price Includes Tax (Y/N)",
            "Medical or Recreational/Adult-use Pricing",
            "URL",
            "Usage (Medical or Recreational)",
            "Strain",
            "Discount Percent",
            "Image URL",
        ];

        //$this->cache = new Vtx_Service_Data_Cache(__CLASS__);
    }

    private function newSession()
    {
        $this->resetCurl(true);

        $this->useProxy = true;
        // $this->setHeaders('Accept-Encoding', 'gzip, deflate, br');
        $this->setHeaders('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8');
        $this->setHeaders('Accept-Language', 'en-US,en;q=0.5');
        $this->setHeaders('Connection', 'keep-alive');
        $this->setHeaders('Upgrade-Insecure-Requests', '1');
        $this->setHeaders('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36');

        $this->setCurlOption(CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        $this->setCurlOption(CURLOPT_CONNECTTIMEOUT, 250);
        $this->setCurlOption(CURLOPT_TIMEOUT, 250);
    }

    public function main($params)
    {

    }

    public function run($dispenId, $slug, $name, $location, $type = '', $pricingType = '')
    {
        $this->newSession();
        $this->switchProxy();
        if ($pricingType) {
            $apiLink = 'https://v3.dutchie.com/graphql?operationName=FilteredProducts&variables={"productsFilter":{"dispensaryId":"' . $dispenId . '","pricingType":"' . $pricingType . '","strainTypes":[],"subcategories":[],"Status":"Active","CBDContent":{"unit":"PERCENTAGE"},"THCContent":{"unit":"PERCENTAGE"},"removeProductsBelowOptionThresholds":true,"types":[],"useCache":true,"sortDirection":1,"sortBy":"salesLast7Days"},"page":0,"perPage":1500}&extensions={"persistedQuery":{"version":1,"sha256Hash":"ab6c6b9e2fd6efcf6c5b89f8e79b6b45740ebc5de7f92a765e451fc303e838b2"}}';
        } else {
            $apiLink = 'https://v3.dutchie.com/graphql?operationName=FilteredProducts&variables={"productsFilter":{"dispensaryId":"' . $dispenId . '","bypassOnlineThresholds":false},"useCache":false}&extensions={"persistedQuery":{"version":1,"sha256Hash":"ab6c6b9e2fd6efcf6c5b89f8e79b6b45740ebc5de7f92a765e451fc303e838b2"}}';
        }

        $detailsAPI = 'https://v3.dutchie.com/graphql?operationName=FilteredProducts&variables={"productsFilter":{"cName":"%s","dispensaryId":"' . $dispenId . '","removeProductsBelowOptionThresholds":true}}&extensions={"persistedQuery":{"version":1,"sha256Hash":"ab6c6b9e2fd6efcf6c5b89f8e79b6b45740ebc5de7f92a765e451fc303e838b2"}}';
        $try = 1;
        do {

            echo("Processing GraphQL API: $try -- $name, $location \n");
            echo("API: $apiLink \n");
            $res = $this->requestGet($apiLink)->getBody();
            $res = $this->jsonDecode($res);
            $products = $res['data']['filteredProducts']['products'];
            if ($products) {
                break;
            }
            $try++;
            $this->newSession();
            $this->switchProxy();
        } while ($try <= 5);

        foreach ($products as $pi => $product) {

            echo("Processing Product: " . ($pi + 1) . " out of " . count($products) . "\n");

            $arr = $this->dataSet->getEmptyRow();

            $arr['Dispensary Name'] = $name;

            $arr['Location Of Dispensary'] = trim(reset(explode('-', $location)));

            $arr['Category'] = $product['type'];

            $arr['Brand Name'] = $product['brandName'];

            $arr['Product Title'] = $product['Name'];

            $arr['Rating'] = "";

            $arr['Number of reviews'] = "";

            $detailUrl = sprintf($detailsAPI, $product['cName']);
            $arr['Description'] = $this->processDescription($detailUrl);
            /**
             * Added: product description by ID, for "Description" found missing in many pulgins!
             * Date: Jan-04-2021
             * By: Anish C.
             */
            if (trim($arr['Description']) == '') {
                li("@@DEscription!");
                $arr['Description'] = $this->processDescriptionByID($product['id'],"https://dutchie.com/embedded-menu/$slug/menu/$product[cName]");
            }

            $arr['Image URL'] = $product['Image'];

            $thcUnit = $product['THCContent']['unit'];
            if ($thcUnit == 'PERCENTAGE') {
                $thcUnit = '%';
            } else if ($thcUnit == 'MILLIGRAMS') {
                $thcUnit = 'mg';
            }

            $thc = trim($product['THCContent']['value'] . ' ' . $thcUnit);
            if ($thc == '%' || $thc == 'mg') {
                $arr['THC'] = '';
            } else {
                $arr['THC'] = $thc;
            }

            $cbdUnit = $product['CBDContent']['unit'];
            if ($cbdUnit == 'PERCENTAGE') {
                $cbdUnit = '%';
            } else if ($cbdUnit == 'MILLIGRAMS') {
                $cbdUnit = 'mg';
            }

            $cbd = trim($product['CBDContent']['value'] . ' ' . $cbdUnit);
            if ($cbd == '%' || $cbd == 'mg') {
                $arr['CBD'] = '';
            } else {
                $arr['CBD'] = $cbd;
            }

            if ($type != '') {
                $arr['Usage (Medical or Recreational)'] = $type;
            } else {
                $arr['Usage (Medical or Recreational)'] = $product['medicalOnly'] ? "Medical" : "Recreational";
            }

            $strain = $product['strainType'];
            $arr['Strain'] = ($strain == 'N/A') ? '' : $strain;

            $arr['URL'] = "https://dutchie.com/embedded-menu/$slug/menu/$product[cName]";

            if ($product['specialData']['percentDiscount']) {
                $arr['Discount Percent'] = $product['specialData']['discount'] . "%";
            }
            foreach ($product['Options'] as $oi => $opt) {
                $wt = $product['Options'][$oi];

                if (preg_match('/([0-9]|\.)/', substr(trim($wt), 0, 1))) {
                    $arr['Weight'] = $wt;
                }

                if ($product['recSpecialPrices'][$oi]) {
                    $arr['Price'] = '$' . $product['recSpecialPrices'][$oi];
                    $arr['Medical or Recreational/Adult-use Pricing'] = '$' . $product['recSpecialPrices'][$oi];
                } else {
                    $arr['Price'] = '$' . $product['Prices'][$oi];
                    $arr['Medical or Recreational/Adult-use Pricing'] = '$' . $product['recPrices'][$oi];
                }

                /**
                 * Added: Percent Discount value found in separate index!
                 * Date: Jan-04-2021
                 * By: Anish C.
                 */
                if(!preg_match('/^[0-9\.]+/',$arr['Discount Percent']) && $product['specialData']['saleSpecials']) {
                    li("@@Discount!");
                    $arr['Discount Percent'] = $product['specialData']['saleSpecials'][$oi]['discount'] . "%";
                }

                if ($arr['Price']) {
//                    print_r($arr);
                    $this->dataSet->addRow($arr);
                }
            }
        }
    }

    private function processDescription($url)
    {
        $try = 1;
        do {
            echo("Retrying for description: $try \n");
            $res = $this->requestGet($url)->getBody();
            $res = $this->jsonDecode($res);
            if ($res) {
                break;
            }
            $try++;
            $this->newSession();
            $this->switchProxy();

        } while ($try <= 5);

        $description = strip_tags($res['data']['filteredProducts']['products'][0]['description']);
        return Vtx_Text_Helper::trimEachLine($description);
    }

    private function processDescriptionByID($productId,$productURL)
    {
        $try = 1;
        do {
            echo("Retrying for description by ID $productId:$productURL $try \n");
            $this->setHeaders('url',$productURL);
            $res = $this->requestGet('https://dutchie.com/graphql?operationName=ProductsQuery&variables=%7B%22productsFilter%22%3A%7B%22productId%22%3A%22' . $productId . '%22%2C%22bypassOnlineThresholds%22%3Afalse%7D%2C%22hideEffects%22%3Afalse%2C%22useCache%22%3Atrue%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22ecafc18ce358baa98e9882f8f6b514a97d8559faef9130501b92164df7411e4c%22%7D%7D')->getBody();
            if($try>2) {
                $res = $this->requestGet('https://dutchie.com/graphql?operationName=ProductsQuery&variables=%7B%22productsFilter%22%3A%7B%22productId%22%3A%22' . $productId . '%22%2C%22bypassOnlineThresholds%22%3Afalse%7D%2C%22hideEffects%22%3Afalse%2C%22useCache%22%3Atrue%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22ab6c6b9e2fd6efcf6c5b89f8e79b6b45740ebc5de7f92a765e451fc303e838b2%22%7D%7D')->getBody();
            }
//            echo "\n RESPONSE: $res";
            $res = $this->jsonDecode($res);
            if (is_array($res['data']['filteredProducts']['products'])) {
                break;
            }
            $try++;
            $this->newSession();
            $this->switchProxy();

        } while ($try <= 5);

        $description = $res['data']['filteredProducts']['products'][0]['Description'];
        return Vtx_Text_Helper::trimEachLine($description);
    }
}